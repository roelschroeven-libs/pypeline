import operator

from pypeline import Pypeline


def example_a():
    (
        # Start with the numbers from 0..4
        Pypeline(range(5))
        # Multiply each number with 3, using a lambda
        .transform(lambda n: n*3)
        # Print intermediate results
        .tee(lambda n: print(f'intermediate: {n}'))
        # Transform each number with 2, using a lambda as transform
        .transform(lambda n: n*2)
        # Filter: keep elements > 1, using a lambda function as precidate
        .where(lambda n: n > 1)
        # Print final results
        .tee(lambda n: print(f'final: {n}'))
        # Trigger evaluation
        .sink()
    )


def example_b():
    # Start with the numbers from 0..4
    p = Pypeline(range(5))
    # Multiply each number with 3, using a lambda
    p.transform(lambda n: n*3)
    # Print intermediate results
    p.tee(lambda n: print(f'intermediate: {n}'))
    # Transform each number with 2, using a lambda as transform
    p.transform(lambda n: n*2)
    # Filter: keep elements > 1, using a lambda function as precidate
    p.where(lambda n: n > 1)
    # Print final results
    p.tee(lambda n: print(f'final: {n}'))
    # Trigger evaluation
    p.sink()


def example_c():
    def multiply(n, multiplier):
        return n * multiplier
    def duplicate_elements(iterable, count=2):
        for element in iterable:
            for _ in range(count):
                yield element
    p = Pypeline(range(5))
    p.transform(multiply, multiplier=10)
    p.apply(duplicate_elements, count=3)
    p.transform(lambda n: n**2)
    p.where(lambda n: n % 2 == 0)
    print(f'sum: {sum(p)}')


def example_d():
    (
        Pypeline(range(5))
        .transform(lambda n: n**2)
        .where(lambda n: n % 2 == 0)
        .tee(print)
        .sink()
    )

def example_e():
    (
        print(
            Pypeline(range(5))
            .transform(lambda n: n**2)
            .reduce(operator.add)
        )
    )

def example_f():
    (
        Pypeline(range(5))
        .transform(lambda n: n**2)
        .where(lambda n: n % 2 == 0)
        .accumulate()
        .tee(print)
        .sink()
    )


if __name__ == '__main__':
    examples = [
        example_a,
        example_b,
        example_c,
        example_d,
        example_f,
    ]
    for example in examples:
        print(f'--- {example.__name__} --')
        example()
        print()

