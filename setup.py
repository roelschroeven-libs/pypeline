#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='Pypeline',
    version='1.0',
    description='Pypeline iterable operations chaining',
    author='Roel Schroeven',
    author_email='roel@roelschroeven.net',
    url='https://gitlab.com/roelschroeven-libs/pypeline.git',
    py_modules=['pypeline'],
)