Pypeline: simple chaining of operations on an iterable
============================================

I like to write some kinds of code as a sequence of operations on an iterable, somewhat
like a pipeline on the Unix command line. There already exists tools to allow that style
of coding, like [pipetools](https://0101.github.io/pipetools/doc). They are more complex
than what I want/need, and I don't need the extra versatility they offer, so I decided
to write my own.

Unlike some (or most, or even all) alternatives, Pypeline is only meant to operate on 
iterables. Supporting other things is not something I need, and will never be supported.

Installation
------------

    pip install git+https://gitlab.com/roelschroeven-libs/pypeline.git

Or just copy [pypeline.py](pypeline.py) into your project.

Documentation
-------------

See docstrings in [pypeline.py](pypeline.py)
