"""
Simple chaining of operations on an iterable.

I like to write some kinds of code as a sequence of operations on an iterable, somewhat
like a pipeline on the Unix command line. There already exists tools to allow that style
of coding, like pipetools [1]. They are more complex than what I want/need, and I don't
need the extra versatility they offer, so I decided to write my own.

[1] https://0101.github.io/pipetools/doc

Notes:

- Pypeline does not use the pipe symbol ('|'), as opposed to some of the alternatives
    and the Unix command line; instead it uses simple method chaining. I feel the pipe
    symbol doesn't offer any added value, and in some respects the use of operator
    overloading makes things unnecessarily more complex. 
- Pypeline is designed to support only iterables. Some (all?) alternatives are more
    general, and allow processing of arbitrary data. That's not something I want to
    focus and, and will never be supported.

Installation
------------

    pip install git+https://gitlab.com/roelschroeven-libs/pypeline.git

Or just copy pypeline.py into your project.

Usage
-----

Initiate a Pypeline instance by calling `Pypeline`(<some iterable>).

Then supply any number of functions that transform the data in some way. There are
two kinds of function you can use with Pypeline:

1. Functions that take an iterable as first argument, and result in a new (or even
    the same) iterator: either they return an iterator, or they work as a generator
    (i.e. they use `yield`). Add these functions using Pypeline's `apply` method.

2. Functions that work on a single element instead of an iterable. Behind the scenes
    these functions will be called for element of the iterable. These functions are
    used with the following methods (see their descriptions below for more info):
    - `where()`: use with a predicate, returning a boolean-like value
    - `transform()`: use with a transform function, returning a transformed version of the element
    - `tee()`: return value is ignored; the element is used unchanged and unfiltered

All functions that you supply to `apply()`, `where()`, `transform()` and `tee()`
optionally take extra arguments and keyword arguments.

Because everything to do with iterables is possibly lazily evaluated, we need a way to 
actually trigger evalaution. There are two ways:

- Use the elements in your code: call `iter()` with the Pypeline instance, and use
  that to iterate over the elements as you would with any other iterable. You don't
  even have to explicitly call `iter()`: in many contexts Python will do it for you.
- Call Pypeline's `sink()` method. This needs to always be the very last thing you
  do  with Pypeline instances, since after that the iterable is not useful anymore.

Examples
--------

The examples assumes the module has been imported using `from pypeline import Pypeline`

Basic example, using a named Pypeline instance. This is especially useful if you want to
use the data outside of Pypeline.

    p = Pypeline(range(5))
    p.transform(lambda n: n**2)
    p.where(lambda n: n % 2 == 0)
    print(f'sum: {sum(p)}')

Basic example, using method chaining. If the code is too complex to fit on a single line,
you'll have to use parens or backslashes to split the code over multiple lines.

    (
        Pypeline(range(5))
        .transform(lambda n: n**2)
        .where(lambda n: n % 2 == 0)
        .tee(print)
        .sink()
    )

Even using this method chaining style, it's possible to use the data outside of Pypeline. 
I think it's clearer to use a named Pypeline instance though.

    p = (
        Pypeline(range(10))
        .transform(lambda n: n**3)
    )
    print(sum(p))

It's also possible to mix and match those styles, in cases where that seems useful.

Function which operates on the full iterator instead of element-by-element:

    def duplicate_elements(iterable, count=2):
        for element in iterable:
            for _ in range(count):
                yield element
    p = Pypeline(range(10))
    p.duplicate_elements(count=10)
    print(sum(p))

Example of combination with itertool functions:

    p = Pypeline(range(10))
    p.apply(functools.partial(itertools.takewhile, lambda n: n < 7))

"""

import functools
import itertools
import operator


EMPTY_INITIALIZER_SENTINEL = object()


class Pypeline:
    """
    Pypeline class: implements simple chaining of operations on an iterable.
    """

    def __init__(self, iterable):
        """
        Create a Pypeline instance
        """
        self.iterable = iterable


    def apply(self, fnc, *extra_args, **kwargs):
        """
        Apply a function that operates on the whole iterable.

        The supplied function takes the current iterable (and optionally *extra_args and 
        **kwargs) and returns a new iterable (or possibly even the same one, but that seems
        hardly useful).
        """
        self.iterable = fnc(self.iterable, *extra_args, **kwargs)
        return self


    def where(self, predicate_fnc, *extra_args, **kwargs):
        """
        Filter based on a predicate function which is called for every element.

        The supplied predicate functions takes an element (and optionally *extra_args and 
        **kwargs) and returns a value which will be evaluated in a boolean context. 
        Only elemens for which that return value evaluate as True are kept.
        """
        return self.apply(functools.partial(filter, functools.partial(predicate_fnc, *extra_args, **kwargs)))

    def transform(self, transform_fnc, *extra_args, **kwargs):
        """
        Transform elements.

        The supplied transform functions takes an element (and optionally *extra_args and 
        **kwargs) and returns a transformed element.
        """
        return self.apply(functools.partial(map, functools.partial(transform_fnc, *extra_args, **kwargs)))

    def tee(self, fnc, *extra_args, **kw_args):
        """
        Call a function for each element without influencing the iterable.

        The supplied transform functions takes an element (and optionally *extra_args and 
        **kwargs); the return value is ignored.

        This can be useful for e.g. printing the elements.
        """
        def wrapped(iterable):
            for element in iterable:
                fnc(element, *extra_args, **kw_args)
                yield element
        return self.apply(wrapped)
    
    def accumulate(self, function=operator.add, initializer=EMPTY_INITIALIZER_SENTINEL):
        """
        Accumulate sums, or accumulated results of other binary functions (specified
        via the optional function argument).
        Usually, the number of elements output matches the input iterable. However,
        if the keyword argument initial is provided, the accumulation leads off with
        the initial value so that the output has one more element than the input
        iterable.
        """
        if initializer is EMPTY_INITIALIZER_SENTINEL:
            self.iterable = itertools.accumulate(self.iterable, function)
        else:
            self.iterable = itertools.accumulate(self.iterable, function, initial=initializer)
        return self
    

    def reduce(self, function, initializer=EMPTY_INITIALIZER_SENTINEL):
        """
        Apply function of two arguments cumulatively to the items of iterable, from left
        to right, so as to reduce the iterable to a single value, which is the return
        value from this function.

        Example: Pypeline(range(5)).reduce(lambda a, b: a+b) calculates (((((0+1)+2)+3)+4)+5)       
        The left argument, x, is the accumulated value and the right argument, y, is
        the update value from the iterable.

        If the optional initializer is present, it is placed before the items of the
        iterable in the calculation, and serves as a default when the iterable is empty.
        If initializer is not given and iterable contains only one item, the first item
        is returned.

        If used, this must be the very last operation.
        """
        if initializer is EMPTY_INITIALIZER_SENTINEL:
            return functools.reduce(function, self.iterable)
        else:
            return functools.reduce(function, self.iterable, initializer)


    def sink(self):
        """
        Trigger evaluation.

        If used, this must be the very last operation. After this, the iterable is not
        useful anymore.
        """
        for _ in iter(self):
            pass


    def __iter__(self):
        """
        Return iterator over the iterable.

        Don't use this directly: instead call `iter`(<Pypeline instance>).
        """
        return iter(self.iterable)

